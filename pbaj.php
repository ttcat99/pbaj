<?php
    /*----------------------------------------------------

    PB&J - Bridge between PHP and JSON
    Author:     ttcat99
    Repository:
        SSH:    git@gitlab.com:ttcat99/pbaj.git
        HTTPS:  https://gitlab.com/ttcat99/pbaj.git
    
    ----------------------------------------------------*/

class PBAJ{

    public function decode(string $json_data){
        return json_decode(mb_convert_encoding($json_data, "UTF-8"), true);
    }

}